package com.example.health.model.medico;

public interface MedicoDTO {
    Long getId();
    String getNome();
    String getCRM();
}
