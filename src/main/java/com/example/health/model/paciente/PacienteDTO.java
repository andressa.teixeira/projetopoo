package com.example.health.model.paciente;

public interface PacienteDTO {
    Long getId();
    String getNome();

}
